﻿#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#define N 10000000

void vector_add(float* out, float* a, float* b, int n) {
    for (int i = 0; i < n; i++) {
        out[i] = a[i] + b[i];
    }
}

int __main__() {

    // Allocate host PC memory
    float* a = (float*)calloc(N,sizeof(float));
    float* b = (float*)calloc(N,sizeof(float));
    float* out = (float*)calloc(N,sizeof(float));

    // Initialize array on host PC
    for (int i = 0; i < N; i++) {
        a[i] = 1.0f; b[i] = 2.0f;
    }

    // Main function
    vector_add(out, a, b, N);

    printf("%f\n", out[0]);
}