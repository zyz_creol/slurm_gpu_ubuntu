﻿#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#define N 100000000

__global__ void vector_add(float* out, float* a, float* b, int n) {
    for (int i = 0; i < n; i++) {
        out[i] = a[i] + b[i];
    }
}

int main() {

    // Allocate host PC memory
    float* a = (float*)calloc(N,sizeof(float));
    float* b = (float*)calloc(N,sizeof(float));
    float* out = (float*)calloc(N,sizeof(float));

    // Initialize array on host PC
    for (int i = 0; i < N; i++) {
        a[i] = 1.0f; b[i] = 2.0f;
    }

    // Allocate device (GPU) memory and copy memory contents from host PC
    float* d_a;
    cudaMalloc((void**)&d_a, N*sizeof(float));
    cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
    float* d_b;
    cudaMalloc((void**)&d_b, N * sizeof(float));
    cudaMemcpy(d_b, b, N * sizeof(float), cudaMemcpyHostToDevice);
    float* d_out;
    cudaMalloc((void**)&d_out, N * sizeof(float));

    // Main function
    vector_add<<<1,1>>>(d_out, d_a, d_b, N);
    cudaMemcpy(out, d_out, N * sizeof(float), cudaMemcpyDeviceToHost);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_out);

    printf("%f\n", out[0]);
}